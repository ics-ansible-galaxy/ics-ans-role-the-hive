# ics-ans-role-the-hive

Ansible role to install the-hive.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-the-hive
```

## License

BSD 2-clause
